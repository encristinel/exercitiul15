package ro.orangetraining;

public class Sender {


    public void send(String message) {

        System.out.println("Sending " + message);
        try {
            Thread.sleep(1000); //sleeps for 1 s
        } catch (InterruptedException e) {
            System.out.println("Interrupted");

        }
        System.out.println(message);
        System.out.println("Sent!");
    }

}
