package ro.orangetraining;

public class ThreadedSend extends Thread {
    private String msg;
    private Thread t;
    Sender sender;

    //Receives a message object and a string message to be sent
    public ThreadedSend(Sender sender, String msg) {
        this.sender = sender;
        this.msg = msg;
    }
    //A method run() that can send only one thread with a message at a time: "synchronized
    public void run() {
        synchronized (sender) {
            sender.send(this.msg);
        }
    }
}